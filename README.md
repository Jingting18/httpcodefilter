# README #

Author: Jingting Zeng
Date: 4/26/2017

Please compile the source code in a standard linux environment, for example:

	bash-3.2$ g++ -std=c++11 -O1 CodeFilter.cpp -o CodeFilter

Please give the file path as the 2nd parameter and httpcode as the 3rd parameter:

	bash-3.2$ ./CodeFilter "u_ex170418.log" "404"
	the ip address: 10.16.4.12 with most code: 404

In the above output, the ip 10.16.4.12 has the most http code 404.

This file can also find other httpcode, for example:

	bash-3.2$ ./CodeFilter "u_ex170418.log" "200"
	the ip address: 127.0.0.1 with most code: 200

	bash-3.2$ ./CodeFilter "u_ex170418.log" "403"
	the ip address: 127.0.0.1 with most code: 403
