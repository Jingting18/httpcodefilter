/*******************************************************
Author: Jingting Zeng
Date: 4/26/2017
Project description: this project implements the 
function to find the ip with the most httpcode
********************************************************/
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <unordered_map>
using namespace std;

class CodeFilter {
private:
	template<typename T>
	void split(const string &, const char, T);
	vector<string> split(const string &, const char);
	bool validCode(const string &, const string &);
	const int ipIndex = 8;
	const int httpCodeIndex = 11;
	const int validDataLen = 12;

public:
	string findMaxHttpCode(const string &, const string &);
};

template<typename T>
void CodeFilter::split(const string &s, const char delim, T result) {
	stringstream ss;
	ss.str(s);
	string item;
	while (getline(ss, item, delim)) {
		*(result++) = item;
	}
}

//split the input string with delimiter.
vector<string> CodeFilter::split(const string &s, const char delim) {
	vector<string> elems;
	split(s, delim, back_inserter(elems));
	return elems;
}

//check whether the source code matches the target code.
bool CodeFilter::validCode(const string &sourceCode, const string &targetCode) {
	return (sourceCode == targetCode);
}

//find the ip which has the maximum number for the given http code
string CodeFilter::findMaxHttpCode(const string &filePath, const string &targetCode) {
	ifstream infile(filePath);
	string line, ipRes;
	int max = 0;
	unordered_map<string, int> codeMap;

	while (getline(infile, line)) {
		vector<string> data = split(line, ' ');

		if ((data.size() > validDataLen)) {
		    string httpCode = data[httpCodeIndex];
		    string ip = data[ipIndex];
		    if (validCode(httpCode, targetCode)) {
			//cout << "ip:" << data[ipIndex] << " targetCode:" << data[httpCodeIndex] << endl;
			int &currVal = codeMap[ip];
			++currVal;
			if (currVal > max) {
			    max = currVal;
			    ipRes = ip;
			}
		    }
		}
	}
	return ipRes;
}

int main(int argc, const char* argv[])
{
	if (argc != 3)
	{
		cout << "Error: requires the file path as 2nd and httpcode as 3rd parameter" << endl;
		return -1;
	}

	string filePath = argv[1];
	string code = argv[2];

	CodeFilter codeFilter;
	string ip = codeFilter.findMaxHttpCode(filePath, code);

	cout << "the ip address: " << ip << " with most code: " << code << endl;
	cout << endl;
	return 0;
}
